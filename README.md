README
------

Building LIBMNL for android
---------------------------

1. Android can be thought of as a different OS than linux. As such, files compiled using linux's compilers (gcc) won't work on android. As such,the first step is to set up a cross-compiler. Android NDK allows you to compile files files/projects for android. We need to set up NDK. Download NDK using wget -c http://dl.google.com/android/ndk/android-ndk-r9b-linux-x86_64.tar.bz2 and extract the files.

2. Android NDK provides "toolchains" (set of tools) to carry out your cross-compilation task. This toolchain is different for different Android architectures. For this project, we need to create a toolchain for x86 architecture. Execute the folloeing:

./android-ndk-r9b/build/tools/make-standalone-toolchain.sh --platform=android-19 --install-dir=/home/sabanerjee/netsec/ndk_standalone --ndk-dir=./android-ndk-r9b --toolchain=x86-4.6

Feel free to change the installation directory as per your needs.

3. Go to the install location (ndk_standalone) and spend a few seconds examining the structure. In the bin folder, you will find android's own version of gcc. This is the gcc you must use to build your code/project. There is also a sysroot folder containing the include files. You will need this folder as well. 

4. You need to make your environment aware of these new compilers. So, set the path. export PATH=/home/sabanerjee/netsec/ndk_standalone/bin:$PATH

5. now clone libmnl. git clone git://git.netfilter.org/libmnl

6. Run ./autogen.sh. You may need to install autoconf and libtool for this to work.

7. Now you will find an executable, "configure". Executing this generates your Makefile. You need to run it with indications that you intend to cross-compile your project for android x86 and provide the path to your toolchain. Execute the following:

./configure --host=i686-linux-android --prefix=/home/sabanerjee/netsec/ndk_standalone/

8. Now run make. This will cry and say:

sabanerjee@sabanerjee-UVM:~/netsec/test/libmnl$ make
make  all-recursive
make[1]: Entering directory `/home/sabanerjee/netsec/test/libmnl'
Making all in src
make[2]: Entering directory `/home/sabanerjee/netsec/test/libmnl/src'
  CC     socket.lo
In file included from ../include/libmnl/libmnl.h:9:0,
                 from socket.c:10:
../include/linux/netlink.h:35:2: error: unknown type name '__kernel_sa_family_t'
make[2]: *** [socket.lo] Error 1
make[2]: Leaving directory `/home/sabanerjee/netsec/test/libmnl/src'
make[1]: *** [all-recursive] Error 1
make[1]: Leaving directory `/home/sabanerjee/netsec/test/libmnl'
make: *** [all] Error 2

You need to make some changes in netlink.h. Change line 35 to 'sa_family_t' instead. Now make will work. 

9. Run "make install" and you are done. 

10. Now go to ndk_standalone and enter the lib folder. You should see the files corresponding to libmnl. Push them to your android device. using, adb push libmnl.so /system/lib. Repeat for other libmnl files. adb may cry that it's a read-only FS. In that case, change it to RW using "mount -o rw,remount /system".

11. Now you may want to see if your libmnl setup on android is OK. For that, write a simple C program to invoke a function of libmnl. Build it using 

make CC=/home/sabanerjee/netsec/ndk_standalone/bin/i686-linux-android-gcc CFLAGS=--sysroot=/home/sabanerjee/netsec/ndk_standalone/sysroot -I/home/sabanerjee/netsec/test/libmnl/include/" LDFLAGS="-L/home/sabanerjee/netsec/ndk_standalone/lib -lmnl" libmnl_test

(Assuming that your file name is libmnl_test.c)

12. once you get the executable, libmnl_test, port it to /data on your android device and execute it. It should execute without errors.

Building libnftnl for android
-----------------------------

1. Follow the similar steps above to clone libnftnl and run ./autogen.sh

2. You need to set a couple of paths before you hit ./configure - 

export LIBMNL_CFLAGS=-I/home/sabanerjee/netsec/test/libmnl/include
export LIBMNL_LIBS=-L/home/sabanerjee/netsec/ndk_standalone/lib

3. ./configure --host=i686-linux-android --prefix=/home/sabanerjee/netsec/ndk_standalone/ 

You will get an error here. The header file, nfnetlink_compat.h will be absent. Add that file in include/linux/netfilter. The contents are available from https://git.netfilter.org/libnetfilter_acct/tree/include/linux/netfilter.

4. make LDFLAGS="-L/home/sabanerjee/netsec/ndk_standalone/lib/ -lmnl"

Note that absence of LDFLAGS is a silent killer. you won't get any errors, but libnftnl won't work. 

5. You will see a few warnings of the form, 

chain.c:364:3: warning: implicit declaration of function 'be64toh' [-Wimplicit-function-declaration]

DO NOT IGNORE THIS! For the files in which you get these warnings, add the following line: #define be64toh(x) betoh64(x)

Then remake.

6. make LDFLAGS="-L/home/sabanerjee/netsec/ndk_standalone/lib/ -lmnl" install

7. Thats it! Now you can write a code to test libnftnl on android. Invoke any function of libnftnl and compile your code as follows: 

make CC=/home/sabanerjee/netsec/ndk_standalone/bin/i686-linux-android-gcc CFLAGS="--sysroot=/home/sabanerjee/netsec/ndk_standalone/sysroot -I/home/sabanerjee/netsec/test/libmnl/include/ -I/home/sabanerjee/netsec/test/libnftnl/include/" LDFLAGS="-L/home/sabanerjee/netsec/ndk_standalone/lib -lmnl -lnftnl" libmnl-nftnl-test

Note that if you miss step 5, you will get a compilation error. 

Building libgmp for android
---------------------------

You can do some cheating here :P The files are are available at https://github.com/Rupan/gmp. The guy built this just a few days back.

Building libreadline for android
--------------------------------

1. Download libreadline-6.2 from http://ftp.gnu.org/gnu/readline/ and make the following change in complete.c:

Look for setpwent() and replace it with:

<hash>if defined (HAVE_GETPWENT)
      setpwent ();
<hash>endif

Alternatively you can download readline-6.3, but for some reason I could not get configure to work properly there. So I stuck to 6.2. 

If you wonder why this is important, read http://lists.gnu.org/archive/html/bug-readline/2012-09/msg00001.html

2. libreadline's configure is pretty outdated and does not support cross-compiling (http://stackoverflow.com/questions/17697779/how-to-get-libreadline-for-android). Replace the configure file:

cp /usr/share/misc/config.{sub,guess} support/.

3. Now run ./configure as usual, followed by make and make install. 

4. Now you might want to  compile the program, test/libreadline-test.c (with LDFLAGS for libreadline set of course) to check if libreadline setup is proper. This will throw a bunch of dependency issues. You basically need ncurses as well. Read on. If you build libncurses as well, you are done.

Building libncurses for Android
-------------------------------

1. Download ncurses: https://code.google.com/p/android-cruft/downloads/detail?name=ncurses-5.7-built-for-android.tar.gz&can=2&q=

2. Same old stories .. build it. You will hit issues during make.

3. As per http://credentiality2.blogspot.com/2010/08/compile-ncurses-for-android.html, set #define HAVE_LOCALE_H 0 in ncurses_cfg.h.

4. Pow make will succeed. Proceed as usual.

Building nftables for Android
-----------------------------

0. CAUTION: If you have built nftables on linux, you might be aware that flex and bison are needed to build it. As such, you may be tempted (like me) to build flex and bison for android and dynamically link their libraries. DO NOT DO THAT. This will require you to add flex to your android device and make your life miserable! 

1. As usual, download the source, git clone git://git.netfilter.org/nftables

2. After autogen, run configure with the following option:
./configure --host=i686-linux-android --prefix=/home/sabanerjee/netsec/ndk_standalone/  CFLAGS="--sysroot=/home/sabanerjee/netsec/ndk_standalone/sysroot -I/home/sabanerjee/netsec/test/libmnl/include/ -I/home/sabanerjee/netsec/test/libnftnl/include/ -I/home/sabanerjee/netsec/test/libgmp/x86 -I/home/sabanerjee/netsec/test/" LDFLAGS="-L/home/sabanerjee/netsec/ndk_standalone/sysroot/usr/lib -L/home/sabanerjee/netsec/ndk_standalone/lib -lmnl -lnftnl -lreadline -lgmp -nostdlib"

Basically this means that you must add paths to all headers of libmnl,libnftnl, etc in CFLAGS and all the libs in LDFLAGS. 

The -nostdlib option is there to prevent the error "C compiler cannot create executables". Not sure how this prevents it. Just looked up. 

3. You will probably see some issues with ip6.h. I replaced the android's version of ip6.h in sysroot with ubuntu's version. Not sure if this will cause issues in future, but I am good for now. Configure will succeed now.

4. Now run make. 

make LDFLAGS="-L/home/sabanerjee/netsec/ndk_standalone/sysroot/usr/lib/ -L/home/sabanerjee/netsec/ndk_standalone/lib -lmnl -lnftnl -lgmp -lreadline -lncurses"

There will be a bunch of conflicts to resolve. As far as I remember, one of them was multiple declaration of ip6_hdr struct. You can comment it out from ipv6.h of your sysroot. Another issue was of some MACROS missing. I think most of them are in icmp6.h which you need to include. There was one more MACRO which needs to be renamed, and a function, strchrnul which needs to be defined in the code which calls it. 

5. make install and you are done. 

6. Copy over src/nft into your android's /sbin/ folder and that's it! Test nft using nft -h

