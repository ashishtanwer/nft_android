#include <gmp.h>
#include <stdio.h>
int main()
{
  mpz_t t;
  mpz_t r1,r2;
  mpz_init(t);
  mpz_init(r1);
  mpz_init(r2);
  mpz_init_set_ui(r1, 123);
  mpz_init_set_str(r2, "123456789123456789000", 10);
  mpz_add(t,r1,r2);
  gmp_printf("%s is an mpz %Zd\n", "here", t);
  mpz_clear(t);
  mpz_clear(r1);
  mpz_clear(r2);
  return 0;
}
